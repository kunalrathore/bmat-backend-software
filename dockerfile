# pull official base image
FROM python:3.9-alpine

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update &&\
    apk add postgresql-dev gcc python3-dev musl-dev

# set up django project
RUN mkdir /bmat-backend-software
WORKDIR /bmat-backend-software
ADD . /bmat-backend-software
RUN pip install -r requirements.txt
EXPOSE 8000
CMD python3 manage.py runserver 0:8000

