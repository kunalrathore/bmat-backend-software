from rest_framework import serializers

from dsrs.models import Territory, Currency, DSR, Record


class TerritorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Territory
        fields = ("name", "code_2")


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ("name", "code")


class DSRSerializer(serializers.ModelSerializer):
    territory = TerritorySerializer()
    currency = CurrencySerializer()

    class Meta:
        model = DSR
        fields = (
            "id",
            "path",
            "period_start",
            "period_end",
            "status",
            "territory",
            "currency",
        )


class RecordSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Record
        fields = "__all__"
