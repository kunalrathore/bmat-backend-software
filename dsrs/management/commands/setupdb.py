import os
import csv
from datetime import datetime

from django.conf import settings
from django.core.management.base import BaseCommand

from dsrs.models import Currency, Territory, DSR, Record


class Command(BaseCommand):
    def handle(self, *args, **options):
        for csvfile in os.listdir(settings.BASE_DIR / "data/tsv"):
            dsp_name, dsp_type, society, territory, currency, period = csvfile.strip(
                ".tsv"
            ).split(
                "_"
            )
            period_start, period_end = period.split("-")
            currency_obj, _ = Currency.objects.get_or_create(
                name=currency, symbol=currency, code=currency
            )
            territory_obj, _ = Territory.objects.get_or_create(
                name=territory,
                code_2=territory,
                code_3=territory,
                local_currency=currency_obj,
            )
            period_start = datetime.strptime(
                period_start, "%Y%m%d"
            ).date()
            period_end = datetime.strptime(period_end, "%Y%m%d").date()
            dsr_obj, _ = DSR.objects.get_or_create(
                path="data/tsv/"+csvfile,
                period_start=period_start,
                period_end=period_end,
                status="failed",
                territory=territory_obj,
                currency=currency_obj,
            )
            record_list = []
            with open("./data/tsv/" + csvfile, "r") as tsv_file:
                csv_reader = csv.reader(tsv_file, delimiter="\t")
                next(csv_reader, None)  # skip the headers
                for row in csv_reader:
                    dsp_id, title, artists, isrc, usages, revenue = row
                    usages = 0 if usages == "" else usages
                    revenue = 0 if revenue == "" else revenue
                        
                    record_list.append(
                        Record(
                            dsp_id=dsp_id,
                            title=title,
                            artists=artists,
                            isrc=isrc,
                            usages=usages,
                            revenue=revenue,
                            dsr=dsr_obj,
                        )
                    )
            Record.objects.bulk_create(
                record_list, ignore_conflicts=True
            )
