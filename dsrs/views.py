import coreapi

from django.db.models import Sum
from rest_framework import viewsets
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend

from dsrs.models import DSR, Record
from dsrs.serializers import RecordSerializer, DSRSerializer


class SimpleFilterBackend(DjangoFilterBackend):
    def get_schema_fields(self, view):
        return [
            coreapi.Field(
                name="territory",
                location="query",
                required=True,
                type="string",
            ),
            coreapi.Field(
                name="period_start",
                location="query",
                required=True,
                type="string",
            ),
            coreapi.Field(
                name="period_end",
                location="query",
                required=True,
                type="string",
            ),
        ]


class DSRViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Get dsr details.

    list:
    An array of DSR in JSON format.
    """
    queryset = DSR.objects.all()
    serializer_class = DSRSerializer
    http_method_names = ["get"]


class GetResource(generics.ListAPIView):
    """ 
    TOP percentile by revenue.
    """
    filter_backends = (SimpleFilterBackend,)
    model = Record
    serializer_class = RecordSerializer

    #  Customized query set to get only top n percetile records by revenue.
    def get_queryset(self, *args, **kwargs):
        number = self.kwargs["number"]
        territory, period_start, period_end = (
            self.request.GET.get("territory", ""),
            self.request.GET.get("period_start", ""),
            self.request.GET.get("period_end", ""),
        )
        filter_records = Record.objects.filter(
            dsr__territory__name=territory,
            dsr__period_start__gte=period_start,
            dsr__period_end__lte=period_end,
        ).order_by("-revenue")
        total_revenue = filter_records.aggregate(Sum("revenue"))
        revenue_percent = (number / 100) * float(
            total_revenue["revenue__sum"]
        )
        result_sum = 0
        count = 0
        for record in filter_records:
            result_sum += record.revenue
            if result_sum > revenue_percent:
                break
            count += 1
        top_percentile_data = filter_records[:count]
        return top_percentile_data
