import json

from django.db.models import Sum
from django.test import TestCase, Client
from rest_framework import status

from dsrs.models import DSR, Record, Currency, Territory
from dsrs.serializers import DSRSerializer, RecordSerializer

# initialize the APIClient app
client = Client()


class GetAllDSR(TestCase):
    """Test module for GET all DSR API"""

    def setUp(self):
        currency_obj = Currency.objects.create(
            name="EUR", symbol="", code=""
        )
        territory_obj = Territory.objects.create(
            name="ES", code_2="", code_3="", local_currency=currency_obj
        )
        dsr_obj = DSR.objects.create(
            path="./data",
            period_start="2020-01-01",
            period_end="2020-01-31",
            status="failed",
            territory=territory_obj,
            currency=currency_obj,
        )
        Record.objects.create(
            dsp_id="amkfldslkdn",
            title="asmksd",
            artists="skdncks",
            isrc="3545t5454",
            usages=345354.34,
            revenue=34534,
            dsr=dsr_obj,
        )

    def test_get_all_dsr(self):
        # get API response
        response = client.get("/dsrs/")
        # get data from db
        dsrs = DSR.objects.all()
        serializer = DSRSerializer(dsrs, many=True)
        # check quality of data
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_top_n_records(self):
        # get API response
        response = client.get(
            "/resources/percentile/10",
            {
                "territory": "ES",
                "period_start": "2020-01-01",
                "period_end": "2020-01-31",
            },
        )
        # get data from db
        filter_records = Record.objects.filter(
            dsr__territory__name="ES",
            dsr__period_start__gte="2020-01-01",
            dsr__period_end__lte="2020-01-31",
        ).order_by("-revenue")
        total_revenue = filter_records.aggregate(Sum("revenue"))
        revenue_percent = (10 / 100) * float(
            total_revenue["revenue__sum"]
        )
        result_sum = 0
        count = 0
        for record in filter_records:
            result_sum += record.revenue
            if result_sum > revenue_percent:
                break
            count += 1
        top_percentile_data = filter_records[:count]
        serializer = RecordSerializer(
            top_percentile_data, many=True
        )
        # check quality of data
        self.assertEqual(
            response.content.decode(),
            json.dumps(serializer.data, separators=(",", ":")),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
