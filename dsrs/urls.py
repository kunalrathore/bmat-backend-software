from django.urls import include, path
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from dsrs.views import GetResource, DSRViewSet

router = routers.DefaultRouter()
router.register(r"dsrs", DSRViewSet)
schema_view = get_swagger_view(title="DSP API")

urlpatterns = [
    path("", include(router.urls)),
    path("resources/percentile/<int:number>", GetResource.as_view()),
    path("swagger/", schema_view),
]
